const functions = require("firebase-functions");
const crypto = require('crypto');
const axios = require('axios').default;

const sharedSecret = "i6MWmM/6KMr5D4CXlg/fBYwCFdbTA5RIJqJqqcJlDdg=";
const bufSecret = Buffer.from(sharedSecret, "base64");


var clientId = "8769a6ed-3886-495b-8467-e6b30114c839"; // "789A5A64-84D8-488B-BABC-FD2937DB12F3"
var clientSecret = "fa-secret-2D7C01FD8FD886D0A326DD"; // "fa-secret-27CC723B1169620F4C5538"; //


async function getToken(clientId, clientSecret) {
  const oAuthOptions = {
    method: 'POST',
    headers: {'content-type': 'application/json'},
    data: `{"grant_type":"client_credentials","client_id": "${clientId}","client_secret": "${clientSecret}"}`,
    url: 'https://try.source.freeagent.network/oauth/token'
  };
  return axios(oAuthOptions)
}

async function postQuery(queryStr,variables, token) {
  const queryOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json', "Authorization" : "Bearer " + token},
    data: JSON.stringify({query: queryStr, variables}),
    url: 'https://try.source.freeagent.network/api/graphql'
  };
  try {
    return await axios(queryOptions);
  } catch (e) {
    return e;
  }
}

var token = null;
var serverDate = null;

async function fetchQuery(queryStr, variables) {
  if (!token) {
    var res = await getToken(clientId, clientSecret);
    token = res.data.access_token;
    serverDate = res.headers.date;
  }
  var queryRes = await postQuery(queryStr,variables, token);
  if (queryRes && queryRes.data && queryRes.data.data) {
    return queryRes.data.data;
  } else {
    return queryRes;
  }
}


// i6MWmM/6KMr5D4CXlg/fBYwCFdbTA5RIJqJqqcJlDdg=
exports.getRecord = functions.https.onRequest((request, response) => {
  functions.logger.info(bufSecret);

  let payload = typeof request.body === "object" ? request.body : JSON.parse(request.body);
  functions.logger.info(payload);
  let responseMsg = '';
  // Process the request

  // Respond to the request
  try {
    // Retrieve authorization HMAC information
    let auth = request.headers['authorization'];
    functions.logger.info(auth);
    // Calculate HMAC on the message we've received using the shared secret
    let msgBuf = Buffer.from(payload, 'utf8');
    let msgHash = "HMAC " + crypto.createHmac('sha256', bufSecret).update(msgBuf).digest('base64');
    console.log("Computed HMAC: " + msgHash);
    console.log("Received HMAC: " + auth);

    if (msgHash === auth) {
      let receivedMsg = payload.text;
      responseMsg = '{ "type": "message", "text": "You typed: ' + receivedMsg.replace('<at>', '') + '" }';
    } else {
      responseMsg = '{ "type": "message", "text": "Error: message sender cannot be authenticated." }';
    }
    return response.send(responseMsg);
  }
  catch (err) {
    return response.send("Error: " + err + "\n" + err.stack);
  }


  /*response.send({
  "$schema": "https://adaptivecards.io/schemas/adaptive-card.json",
  "type": "AdaptiveCard",
  "version": "1.0",
  "style": "emphasis",
  "body": [
    {
      "type": "Container",
      "style": "emphasis",
      "items": [
        {
          "type": "ColumnSet",
          "columns": [
            {
              "width": "auto",
              "items": [
                {
                  "type": "Image",
                  "style": "person",
                  "width": "50px",
                  "url": "https://connectorsdemo.azurewebsites.net/images/MSC12_Oscar_002.jpg",
                  "altText": "Miguel Garcia's Profile Picture"
                }
              ]
            },
            {
              "width": "stretch",
              "spacing": "padding",
              "items": [
                {
                  "type": "TextBlock",
                  "size": "large",
                  "color": "accent",
                  "text": "**New opportunity created**",
                  "wrap": true
                },
                {
                  "type": "TextBlock",
                  "spacing": "none",
                  "text": "by Miguel Garcia on {{DATE(2017-09-20T16:00:00Z,LONG)}}",
                  "wrap": true
                }
              ]
            }
          ]
        },
        {
          "type": "Container",
          "spacing": "padding",
          "style": "default",
          "items": [
            {
              "type": "TextBlock",
              "text": "**Title:**"
            },
            {
              "type": "Input.Text",
              "id": "title",
              "value": "Brown Industries time machine.",
              "placeholder": "Enter title"
            },
            {
              "type": "TextBlock",
              "text": "**Description:**"
            },
            {
              "type": "Input.Text",
              "id": "description",
              "isMultiline": true,
              "value": "Brown Industries is looking for an affordable Flux Capacitor that can be mass produced. Big potential. It's us vs Tannen Corp,. at this point, but our product is smaller and fits better in a DeLorean.",
              "placeholder": "Enter description"
            },
            {
              "type": "ColumnSet",
              "columns": [
                {
                  "width": 1,
                  "items": [
                    {
                      "type": "TextBlock",
                      "text": "**Start date:**"
                    },
                    {
                      "type": "Input.Date",
                      "id": "startDate",
                      "value": "2017-09-20",
                      "placeholder": "Enter start date"
                    }
                  ]
                },
                {
                  "width": 1,
                  "items": [
                    {
                      "type": "TextBlock",
                      "text": "**End date:**"
                    },
                    {
                      "type": "Input.Date",
                      "id": "endDate",
                      "value": "2018-01-01",
                      "placeholder": "Enter end date"
                    }
                  ]
                }
              ]
            },
            {
              "type": "ColumnSet",
              "columns": [
                {
                  "width": 1,
                  "items": [
                    {
                      "type": "TextBlock",
                      "text": "**Amount:**"
                    },
                    {
                      "type": "Input.Number",
                      "id": "amount",
                      "value": 1000000000,
                      "placeholder": "Enter amount"
                    }
                  ]
                },
                {
                  "width": 1,
                  "items": [
                    {
                      "type": "TextBlock",
                      "text": "**Stage:**"
                    },
                    {
                      "type": "Input.ChoiceSet",
                      "id": "stage",
                      "choices": [
                        {
                          "title": "Prospecting",
                          "value": "1"
                        },
                        {
                          "title": "Qualification",
                          "value": "2"
                        },
                        {
                          "title": "Negotiation Review",
                          "value": "3"
                        },
                        {
                          "title": "Closed Won",
                          "value": "4"
                        },
                        {
                          "title": "Closed Loss",
                          "value": "5"
                        }
                      ],
                      "value": "1",
                      "placeholder": "Select Stage"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "Container",
          "id": "tasks",
          "spacing": "padding",
          "style": "default",
          "isVisible": true,
          "items": [
            {
              "type": "TextBlock",
              "text": "**Tasks**"
            },
            {
              "type": "Input.ChoiceSet",
              "id": "tasks",
              "isMultiSelect": true,
              "choices": [
                {
                  "title": "**[Completed]** Confirm integration feasibility in DeLorean",
                  "value": "1"
                },
                {
                  "title": "Call Dr. Brown to negotiate price",
                  "value": "2"
                },
                {
                  "title": "Setup test date with Marty McFly",
                  "value": "2"
                }
              ],
              "value": "1"
            },
            {
              "type": "ColumnSet",
              "columns": [
                {
                  "width": "stretch",
                  "items": [
                    {
                      "type": "Input.Text",
                      "id": "newTask",
                      "isRequired": true,
                      "placeholder": "Enter a new task"
                    }
                  ]
                },
                {
                  "width": "auto",
                  "verticalContentAlignment": "center",
                  "items": [
                    {
                      "type": "ActionSet",
                      "actions": [
                        {
                          "type": "Action.Http",
                          "method": "POST",
                          "body": "{{newTask.value}}",
                          "id": "addTask",
                          "title": "Add task",
                          "url": "https://messagecardplaygroundfn.azurewebsites.net/"
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "ActionSet",
          "spacing": "padding",
          "actions": [
            {
              "type": "Action.Http",
              "method": "POST",
              "body": "{}",
              "id": "update",
              "title": "Update opportunity",
              "url": "https://messagecardplaygroundfn.azurewebsites.net/"
            },
            {
              "type": "Action.OpenUrl",
              "id": "open",
              "title": "Open opportunity",
              "url": "https://microsoft.com"
            }
          ]
        }
      ]
    }
  ]
});*/


});


exports.faRecord = functions.https.onRequest(async (request, response) => {
  functions.logger.info(bufSecret);
  let payload = typeof request.body === "object" ? request.body : JSON.parse(request.body);
  functions.logger.info(payload.text);
  let searchText = payload.text.split('</at>')[1].replace(/(^\s+|\s+$)/g,'').replace('\n');
  functions.logger.info(searchText);


  let queryStr = `query listEntityValues($entity: String!, $fields: [String], $order: [[String]], $limit: Int, $offset: Int, $pattern: String, $filters: [Filter]) {
    listEntityValues(entity: $entity, fields: $fields, order: $order, limit: $limit, offset: $offset, pattern: $pattern, filters: $filters) {
      count
      entity_values {
        id
        seq_id
        field_values
      }
    }
  }`;

  let variables = {
    "entity" : "contact",
    "fields" : ["seq_id","full_name","work_email","work_phone"],
    "limit" : 1,
    "filters" : [
      {
        "field_name" : "full_name",
        "operator" : "contains",
        "values" : [searchText]
      }
    ],
  }
  let res = await fetchQuery(queryStr, variables);

  functions.logger.info(res);

  let info = res && res.listEntityValues ? res.listEntityValues.entity_values[0] : null;


  let { seq_id, full_name, work_email, work_phone } = info.field_values;



  response.send(JSON.stringify({
    type: 'message',
    text: `FA Contact Found:`,
    attachments: [
      {
        contentType: "application/vnd.microsoft.card.adaptive",
        content: {
          "type": "AdaptiveCard",
          "version": "1.0",
          "body": [
            {
              "type": "TextBlock",
              "text": seq_id.value,
              "size": "large"
            },
            {
              "type": "TextBlock",
              "text": full_name.value
            },
            {
              "type": "TextBlock",
              "text": work_email.value,
              "separation": "none"
            },
            {
              "type": "TextBlock",
              "text": work_phone.value,
              "separation": "none"
            }
          ],
          "actions": [
            {
              "type": "Action.OpenUrl",
              "url": `https://try.source.freeagent.network/contact/view/${info.id}`,
              "title": "Contact Record in FA"
            }
          ]
        }
      }
    ]
  }));

});



exports.createRecord = functions.https.onRequest(async (request, response) => {

  let secret = 'Xv7SIxvPaEy6q59dIenbzBTpQY22+z1gIqakcI6RM68=';

  let payload = typeof request.body === "object" ? request.body : JSON.parse(request.body);


  let queryStr = `query listEntityValues($entity: String!, $fields: [String], $order: [[String]], $limit: Int, $offset: Int, $pattern: String, $filters: [Filter]) {
    listEntityValues(entity: $entity, fields: $fields, order: $order, limit: $limit, offset: $offset, pattern: $pattern, filters: $filters) {
      count
      entity_values {
        id
        seq_id
        field_values
      }
    }
  }`;

  /*let variables = {
    "entity" : "contact",
    "fields" : ["seq_id","full_name","work_email","work_phone"],
    "limit" : 1,
    "filters" : [
      {
        "field_name" : "full_name",
        "operator" : "contains",
        "values" : ["searchText"]
      }
    ],
  }

  let res = await fetchQuery(queryStr, variables);*/


  response.send(JSON.stringify({
    type: 'message',
    text: `Lead Form`,
    attachments: [
      {
        contentType: "application/vnd.microsoft.card.adaptive",
        content: {
          "type": "AdaptiveCard",
          "body": [
            {
              "type": "Container",
              "spacing": "None",
              "style": "emphasis",
              "items": [
                {
                  "type": "ColumnSet",
                  "columns": [
                    {
                      "type": "Column",
                      "items": [
                        {
                          "type": "TextBlock",
                          "text": "Event Feedback"
                        }
                      ],
                      "width": "stretch",
                      "padding": "None"
                    },
                    {
                      "type": "Column",
                      "items": [
                        {
                          "type": "Image",
                          "id": "4b3986aa-ca62-8df9-f4c9-5d9bc8585978",
                          "url": "https://amdesigner.azurewebsites.net/samples/assets/Contoso_Logo.png",
                          "horizontalAlignment": "Right",
                          "height": "12px"
                        }
                      ],
                      "width": "auto",
                      "verticalContentAlignment": "Center",
                      "padding": "None"
                    }
                  ],
                  "padding": "None"
                }
              ],
              "padding": "Default"
            },
            {
              "type": "Container",
              "items": [
                {
                  "type": "TextBlock",
                  "size": "Large",
                  "weight": "Bolder",
                  "text": "Help us improve by taking our short satisfaction survey.",
                  "wrap": true
                }
              ],
              "padding": "Default",
              "spacing": "None",
              "separator": true
            },
            {
              "type": "Container",
              "items": [
                {
                  "type": "TextBlock",
                  "weight": "Bolder",
                  "text": "1\\. How likely is that you would recommend the event to a friend or colleague?",
                  "wrap": true
                },
                {
                  "type": "Input.ChoiceSet",
                  "id": "Recommendation",
                  "placeholder": "Select rating",
                  "choices": [
                    {
                      "title": "1",
                      "value": "1"
                    },
                    {
                      "title": "2",
                      "value": "2"
                    },
                    {
                      "title": "3",
                      "value": "3"
                    },
                    {
                      "title": "4",
                      "value": "4"
                    },
                    {
                      "title": "5",
                      "value": "5"
                    }
                  ],
                  "isRequired": true
                },
                {
                  "type": "TextBlock",
                  "weight": "Bolder",
                  "text": "2\\. Overall, how would you rate the event?",
                  "wrap": true,
                  "spacing": "Large"
                },
                {
                  "type": "Input.ChoiceSet",
                  "id": "OverallRating",
                  "choices": [
                    {
                      "title": "Excellent",
                      "value": "Excellent"
                    },
                    {
                      "title": "Very Good",
                      "value": "Very Good"
                    },
                    {
                      "title": "Good",
                      "value": "Good"
                    },
                    {
                      "title": "Fair",
                      "value": "Fair"
                    },
                    {
                      "title": "Poor",
                      "value": "Poor"
                    }
                  ],
                  "style": "expanded",
                  "isRequired": true
                },
                {
                  "type": "TextBlock",
                  "weight": "Bolder",
                  "text": "3\\. What did you like about the event?",
                  "wrap": true,
                  "spacing": "Large"
                },
                {
                  "type": "Input.Text",
                  "id": "Comment",
                  "placeholder": "Tell us everything",
                  "isMultiline": true
                },
                {
                  "type": "TextBlock",
                  "weight": "Bolder",
                  "text": "4\\. When were you informed about the event?",
                  "wrap": true,
                  "spacing": "Large"
                },
                {
                  "type": "Input.Date",
                  "id": "InformedDate",
                  "isRequired": true
                },
                {
                  "type": "TextBlock",
                  "weight": "Bolder",
                  "text": "5\\. How would you describe the event?",
                  "wrap": true,
                  "spacing": "Large"
                },
                {
                  "type": "Input.ChoiceSet",
                  "id": "Feeling",
                  "choices": [
                    {
                      "title": "Organized",
                      "value": "Organized"
                    },
                    {
                      "title": "Enjoyable",
                      "value": "Enjoyable"
                    },
                    {
                      "title": "Informative",
                      "value": "Informative"
                    },
                    {
                      "title": "Relevant",
                      "value": "Relevant"
                    },
                    {
                      "title": "Social",
                      "value": "Social"
                    }
                  ],
                  "style": "expanded",
                  "isMultiSelect": true,
                  "isRequired": true
                },
                {
                  "type": "ActionSet",
                  "actions": [
                    {
                      "type": "Action.OpenUrl",
                      "title": "Open Url",
                      "url": "https://34bdeebae303.ngrok.io/?text={{Comment.value}}",
                    }
                  ]
                }
              ],
              "padding": {
                "top": "Small",
                "bottom": "Default",
                "left": "Default",
                "right": "Default"
              },
              "spacing": "None"
            }
          ],
          "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
          "version": "1.0",
          "padding": "None"
        }
      }]
  }));
});


exports.generateLead = functions.https.onRequest(async (request, response) => {

  let secret = 'Xv7SIxvPaEy6q59dIenbzBTpQY22+z1gIqakcI6RM68=';

  let payload = typeof request.body === "object" ? request.body : JSON.parse(request.body);
  functions.logger.info(payload, {structured:true});

  let queryStr = `query listEntityValues($entity: String!, $fields: [String], $order: [[String]], $limit: Int, $offset: Int, $pattern: String, $filters: [Filter]) {
    listEntityValues(entity: $entity, fields: $fields, order: $order, limit: $limit, offset: $offset, pattern: $pattern, filters: $filters) {
      count
      entity_values {
        id
        seq_id
        field_values
      }
    }
  }`;



  let url = "https://us-central1-maxlite-demo.cloudfunctions.net/proxyRequestApp/query";


  //let url = "http://localhost:5002/maxlite-demo/us-central1/proxyRequestApp/query";

  let { name, employees, location_name, first_name, last_name, work_email, work_phone } = payload;

  let contactVariables = {
    entity: 'contact',
    field_values: {
      first_name,
      last_name,
      work_email,
      work_phone
    }
  }

  let logoVariables = {
    entity: 'logo',
    field_values: {
      name,
      num_employees: parseInt(employees),
      hq_location: location_name
    }
  }


  let creatEntityQuery = `mutation createEntity($entity: String!, $field_values: JSON!) {
                                    createEntity(entity: $entity, field_values: $field_values) {
                                      entity_value {
                                        id
                                        seq_id
                                        field_values
                                      }
                                    }
                                  }`



  let contactRes = await fetchQuery(creatEntityQuery, contactVariables);


  functions.logger.info(contactRes);



  if (contactRes && contactRes.createEntity) {
    let contactId = contactRes.createEntity.entity_value.id;
    logoVariables.field_values.logo_field77 = contactId;
    functions.logger.info(contactId);
  }

  let logoRes = await fetchQuery(creatEntityQuery, logoVariables);

  functions.logger.info(logoRes);

  /*let variables = {
    "entity" : "contact",
    "fields" : ["seq_id","full_name","work_email","work_phone"],
    "limit" : 1,
    "filters" : [
      {
        "field_name" : "full_name",
        "operator" : "contains",
        "values" : ["searchText"]
      }
    ],
  }

  let res = await fetchQuery(queryStr, variables);*/


  response.send(JSON.stringify({
    type: "AdaptiveCard",
    version: "1.0",
    body: [
      {
        "type": "TextBlock",
        "text": `Lead and Logo have been created successfully`,
        "size": "medium"
      },
    ],
    "actions": [
      {
        "type": "Action.ShowCard",
        "title": `Lead: ${first_name} ${last_name}`,
        "card": {
          "type": "AdaptiveCard",
          "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
          "body": [
            {
              "type": "TextBlock",
              "text": contactRes.createEntity.entity_value.seq_id,
              "size": "large"
            },
            {
              "type": "TextBlock",
              "text": `Full Name: ${first_name} ${last_name}`
            },
            {
              "type": "TextBlock",
              "text": `Email: ${work_email}`,
              "separation": "none"
            },
            {
              "type": "TextBlock",
              "text": `Phone: ${work_phone}`,
              "separation": "none",
            },
            {
              "type": "ActionSet",
              "actions": [
                {
                  "type": "Action.OpenUrl",
                  "title": "Open Lead in FA",
                  "url": `https://try.source.freeagent.network/contact/view/${contactRes.createEntity.entity_value.id}`,
                }
              ]
            }
            ]
        }
      },
      {
        "type": "Action.ShowCard",
        "title": `Logo: ${name}`,
        "card": {
          "type": "AdaptiveCard",
          "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
          "body": [
            {
              "type": "TextBlock",
              "text": logoRes.createEntity.entity_value.seq_id,
              "size": "large"
            },
            {
              "type": "TextBlock",
              "text": `Company Name: ${name}`
            },
            {
              "type": "TextBlock",
              "text": `Address: ${location_name}`,
              "separation": "none"
            },
            {
              "type": "TextBlock",
              "text": `Number Of Employees: ${employees}`,
              "separation": "none",
            },
            {
              "type": "ActionSet",
              "actions": [
                {
                  "type": "Action.OpenUrl",
                  "title": "Open Logo in FA",
                  "url": `https://try.source.freeagent.network/logo/view/${logoRes.createEntity.entity_value.id}`,
                }
              ]
            }
          ]
        }
      }
    ]
  }));
});


exports.messageCard = functions.https.onRequest(async (request, response) => {
  // MjgDLhtF531SJlssO8AVhb0EZCDX/aB1nTX3Zv+Hu4I=
   response.send({
     "@context": "https://schema.org/extensions",
     "@type": "MessageCard",
     "themeColor": "0072C6",
     "title": "Visit the Outlook Dev Portal",
     "text": "Click **Learn More** to learn more about Actionable Messages!",
     "potentialAction": [
       {
         "@type": "ActionCard",
         "name": "Send Feedback",
         "inputs": [
           {
             "@type": "TextInput",
             "id": "feedback",
             "isMultiline": true,
             "title": "Let us know what you think about Actionable Messages"
           }
         ],
         "actions": [
           {
             "@type": "HttpPOST",
             "name": "Send Feedback",
             "isPrimary": true,
             "target": "https://a59065e3c02e.ngrok.io"
           }
         ]
       },
       {
         "@type": "OpenUri",
         "name": "Learn More",
         "targets": [
           { "os": "default", "uri": "https://docs.microsoft.com/outlook/actionable-messages" }
         ]
       }
     ]
   })
});


exports.messageCardResponse = functions.https.onRequest(async (request, response) => {
    functions.logger.info(request.query);
   response.send({text:"ok"});
});






