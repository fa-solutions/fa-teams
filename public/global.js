/*
let originUrl = 'https://4797e58da686.ngrok.io';
let clientId = '7f650e4c-00dc-42fb-9a3a-ea28fd9556cb';
const callback = (errorDesc, token, error, tokenType) => {};
// An Optional options for initializing the MSAL @see https://github.com/AzureAD/microsoft-authentication-library-for-js/wiki/MSAL-basics#configuration-options
const options = {
    redirectUri: originUrl,
};
const graphScopes = ["user.read", "mail.send", "ChannelMessage.Send", "Group.ReadWrite.All"]; // An array of graph scopes

// Initialize the MSAL @see https://github.com/AzureAD/microsoft-authentication-library-for-js/wiki/MSAL-basics#initialization-of-msal
const userAgentApplication = new UserAgentApplication(clientId, undefined, callback, options);
const authProvider = new MSALAuthenticationProvider(userAgentApplication, graphScopes );
const options = {
    authProvider, // An instance created from previous step
};
const Client = MicrosoftGraph.Client;
const client = Client.initWithMiddleware(options);

const chatMessage = {
    body: {
        contentType: "html",
        content: "Hello World"
    }
};

let res = await client.api('/teams/{id}/channels/{id}/messages/{id}/replies')
    .post(chatMessage);


const user = await client
    .api('/me')
    .version('beta')
    .get();

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

let token = getCookie(`msal.${clientId}.idtoken`);

console.log(token);

async function postToGraph(url="/beta/planner/tasks", method='post', data={}) {
    let options = {
        url,
        method,
        baseURL: 'graph.microsoft.com',
        headers: {
            Authorization: `Bearer ${token}`,
        },
        data: {
            assignments: {
                '1ccfe6ba-c762-49dd-a424-ee461c8d13f1': {
                    '@odata.type': "microsoft.graph.plannerAssignment",
                    orderHint: "string !"
                }
            },
            bucketId: "z4jtQ-CxBkymv2ysTeq7F2UAILx0",
            dueDateTime: "2021-02-18T23:00:00.000Z",
            planId: "cx3hcF0QCE-zgYcVtlNusGUAF_VE",
            title: "New"
        },
    }
    let res = await axios(options);
    console.log(res);
}

postToGraph();
*/

/*
let headers = {

    POST: '/beta/planner/tasks HTTP/1.1',
    Host: 'graph.microsoft.com',
    Connection: 'keep-alive',
    Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJub25jZSI6IkZMMUhWWkFORHdGbU1qZGhYMDhNYmx0d0tMeG55QjJFNmk3clFzN0Q3Y1UiLCJhbGciOiJSUzI1NiIsIng1dCI6Im5PbzNaRHJPRFhFSzFqS1doWHNsSFJfS1hFZyIsImtpZCI6Im5PbzNaRHJPRFhFSzFqS1doWHNsSFJfS1hFZyJ9.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTAwMDAtYzAwMC0wMDAwMDAwMDAwMDAiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC84ZDI1ZWJlNC0zMzcyLTQxYjEtYTM3Mi0zOGJiODEwODdlNGMvIiwiaWF0IjoxNjEzMDc0NzM3LCJuYmYiOjE2MTMwNzQ3MzcsImV4cCI6MTYxMzA3ODYzNywiYWNjdCI6MCwiYWNyIjoiMSIsImFjcnMiOlsidXJuOnVzZXI6cmVnaXN0ZXJzZWN1cml0eWluZm8iLCJ1cm46bWljcm9zb2Z0OnJlcTEiLCJ1cm46bWljcm9zb2Z0OnJlcTIiLCJ1cm46bWljcm9zb2Z0OnJlcTMiLCJjMSIsImMyIiwiYzMiLCJjNCIsImM1IiwiYzYiLCJjNyIsImM4IiwiYzkiLCJjMTAiLCJjMTEiLCJjMTIiLCJjMTMiLCJjMTQiLCJjMTUiLCJjMTYiLCJjMTciLCJjMTgiLCJjMTkiLCJjMjAiLCJjMjEiLCJjMjIiLCJjMjMiLCJjMjQiLCJjMjUiXSwiYWlvIjoiQVVRQXUvOFRBQUFBYjhjTWg1aFhOekJjSW9IWHBibWF2SDN3SUYxQUEwZVgydU5iY0YyN1RISStEK3lhWTlnMUxZNHlVL3RBQ0ZqN0grUDJyK3ZSVS9qaVZGWmJSLzVTenc9PSIsImFtciI6WyJwd2QiLCJtZmEiXSwiYXBwX2Rpc3BsYXluYW1lIjoiR3JhcGggMzY1IFRlc3QiLCJhcHBpZCI6IjdmNjUwZTRjLTAwZGMtNDJmYi05YTNhLWVhMjhmZDk1NTZjYiIsImFwcGlkYWNyIjoiMCIsImZhbWlseV9uYW1lIjoiTWloYWpsb3ZpYyIsImdpdmVuX25hbWUiOiJTbG9ib2RhbiIsImhhc3dpZHMiOiJ0cnVlIiwiaWR0eXAiOiJ1c2VyIiwiaXBhZGRyIjoiMTg3LjE0NC4yNDcuMTI0IiwibmFtZSI6IlNsb2JvZGFuIE1paGFqbG92aWMiLCJvaWQiOiIxY2NmZTZiYS1jNzYyLTQ5ZGQtYTQyNC1lZTQ2MWM4ZDEzZjEiLCJwbGF0ZiI6IjUiLCJwdWlkIjoiMTAwMzIwMDEwQjRBRDlDRCIsInJoIjoiMC5BQUFBNU9zbGpYSXpzVUdqY2ppN2dRaC1URXdPWlhfY0FQdENtanJxS1AyVlZzdDRBQkEuIiwic2NwIjoiQXBwbGljYXRpb24uUmVhZFdyaXRlLkFsbCBDYWxlbmRhcnMuUmVhZCBDYWxlbmRhcnMuUmVhZFdyaXRlLlNoYXJlZCBlbWFpbCBHcm91cC5SZWFkLkFsbCBHcm91cC5SZWFkV3JpdGUuQWxsIE1haWwuUmVhZCBNYWlsLlJlYWRCYXNpYyBNYWlsLlJlYWRXcml0ZS5TaGFyZWQgTWFpbC5TZW5kLlNoYXJlZCBvcGVuaWQgUGVvcGxlLlJlYWQgUGVvcGxlLlJlYWQuQWxsIHByb2ZpbGUgU2l0ZXMuUmVhZC5BbGwgVGFza3MuUmVhZCBUYXNrcy5SZWFkV3JpdGUgVXNlci5SZWFkIFVzZXIuUmVhZC5BbGwgVXNlci5SZWFkQmFzaWMuQWxsIiwic2lnbmluX3N0YXRlIjpbImttc2kiXSwic3ViIjoiUjNnQkZlQzhpQVBzNVRqLWxBb2pMcjhzekVrVDRGeFdXZjJraEdLSl9xUSIsInRlbmFudF9yZWdpb25fc2NvcGUiOiJOQSIsInRpZCI6IjhkMjVlYmU0LTMzNzItNDFiMS1hMzcyLTM4YmI4MTA4N2U0YyIsInVuaXF1ZV9uYW1lIjoiYm9iYW5icmJhbkBmYXNvbHV0aW9uc3RlYW0ub25taWNyb3NvZnQuY29tIiwidXBuIjoiYm9iYW5icmJhbkBmYXNvbHV0aW9uc3RlYW0ub25taWNyb3NvZnQuY29tIiwidXRpIjoiX2lVWEllNzI3a0dUa1o5OTV2SW9BQSIsInZlciI6IjEuMCIsInhtc19zdCI6eyJzdWIiOiItNWxnd0lMSHo0eEx6MHN1VHFWWjJnN3Q0Q2JPMWllYVlDelYzV1VMRVljIn0sInhtc190Y2R0IjoxNjEwNDAzOTYzfQ.EjACGAWEu-Xc0k41R1TEJkFs75VxUbJD9eDgox_FWp2xpLhLCyEpGSEhR1O0s8X2qvoFw7VIAOd3jcwC2U_7qnS1XsVYNpY528jsTCZsUIONMk5IbS8c16qCJKauvcCnH6hu8TinzyYj4AbBPhMHuW3oVfORuERhD9WyU6VEqy-H4AnQZ0u6JaLK2HBoUzASiJ7wAQwO6Bjv-GczZ-BQtO_in7k8cRY5kH6Yn60ERj28sttzp8A779NWTTSKZfSgv6wg1V4PQ6rk7a0wMwTn0lGNL0w2PW3psRodLmIgNJKC7mXUC_lMLeejV_LSi7HY2tVTySDOQDLbNh-y1RPK8w",
    'client-request-id': 'f0af8ec7-dd30-c094-f818-740b94f21bfa',
    'Content-Type: application/json',
    'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36
    Origin: https://4797e58da686.ngrok.io
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Sec-Fetch-Dest: empty
Referer: https://4797e58da686.ngrok.io/
    Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9
}
*/
